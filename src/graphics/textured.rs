use super::WebGlConsts;
use crate::{app::ApplicationState, util, WebGl};
use anyhow::{anyhow, Context, Result};
use bytemuck::{Pod, Zeroable};
use glam::{Mat4, Vec2, Vec3, Vec4};
use memoffset::offset_of;
use std::{convert::TryInto, mem, rc::Rc};
use tobj::LoadOptions;
use wasm_bindgen::{prelude::Closure, JsCast};
use web_sys::{HtmlImageElement, WebGlBuffer, WebGlProgram, WebGlTexture, WebGlUniformLocation};
use winit::dpi::PhysicalSize;

#[repr(C)]
#[derive(Clone, Copy)]
struct Vertex {
    position: Vec3,
    normal: Vec3,
    uv: Vec2,
}

unsafe impl Zeroable for Vertex {}
unsafe impl Pod for Vertex {}

impl Vertex {
    const SIZE: usize = mem::size_of::<Self>();

    fn apply_vertex_attrib_layout(
        webgl: &WebGl,
        program: &WebGlProgram,
        vertex_buffer: &WebGlBuffer,
    ) {
        webgl.bind_buffer(WebGlConsts::ARRAY_BUFFER, Some(vertex_buffer));

        let position = webgl.get_attrib_location(&program, "position");
        webgl.enable_vertex_attrib_array(position as _);
        webgl.vertex_attrib_pointer_with_i32(
            position as _,
            3,
            WebGlConsts::FLOAT,
            false,
            Self::SIZE as _,
            offset_of!(Vertex, position) as _,
        );
        webgl
            .angle_instanced_arrays
            .vertex_attrib_divisor_angle(position as _, 0);

        let normal = webgl.get_attrib_location(&program, "normal");
        webgl.enable_vertex_attrib_array(normal as _);
        webgl.vertex_attrib_pointer_with_i32(
            normal as _,
            3,
            WebGlConsts::FLOAT,
            false,
            Self::SIZE as _,
            offset_of!(Vertex, normal) as _,
        );
        webgl
            .angle_instanced_arrays
            .vertex_attrib_divisor_angle(normal as _, 0);

        let uv = webgl.get_attrib_location(&program, "uv");
        webgl.enable_vertex_attrib_array(uv as _);
        webgl.vertex_attrib_pointer_with_i32(
            uv as _,
            2,
            WebGlConsts::FLOAT,
            false,
            Self::SIZE as _,
            offset_of!(Vertex, uv) as _,
        );
        webgl
            .angle_instanced_arrays
            .vertex_attrib_divisor_angle(uv as _, 0);
    }
}

#[repr(C)]
#[derive(Clone, Copy)]
struct Instance {
    model: Mat4,
}

unsafe impl Zeroable for Instance {}
unsafe impl Pod for Instance {}

impl Instance {
    const SIZE: usize = mem::size_of::<Self>();

    fn apply_vertex_attrib_layout(
        webgl: &WebGl,
        program: &WebGlProgram,
        instance_buffer: &WebGlBuffer,
        instances: &[Instance],
    ) {
        webgl.bind_buffer(WebGlConsts::ARRAY_BUFFER, Some(instance_buffer));
        webgl.buffer_data_with_u8_array(
            WebGlConsts::ARRAY_BUFFER,
            bytemuck::cast_slice(&instances),
            WebGlConsts::DYNAMIC_DRAW,
        );

        let model = webgl.get_attrib_location(&program, "model");
        for i in 0..4 {
            let model = model + i;
            webgl.enable_vertex_attrib_array(model as _);
            let offset = mem::size_of::<Vec4>() as i32 * i;
            webgl.vertex_attrib_pointer_with_i32(
                model as _,
                4,
                WebGlConsts::FLOAT,
                false,
                Self::SIZE as _,
                offset_of!(Instance, model) as i32 + offset,
            );

            webgl
                .angle_instanced_arrays
                .vertex_attrib_divisor_angle(model as _, 1);
        }
    }
}

pub struct TexturedModel {
    vertex_buffer: WebGlBuffer,
    indices_count: i32,
    index_buffer: WebGlBuffer,
    texture: WebGlTexture,
}

impl TexturedModel {
    pub fn from_obj(
        webgl: &WebGl,
        mut obj: &[u8],
        hosted_texture_path: &str,
        mipmapping: bool,
    ) -> Result<TexturedModel> {
        let (models, _materials) = tobj::load_obj_buf(
            &mut obj,
            &LoadOptions {
                triangulate: true,
                ignore_points: true,
                ignore_lines: true,
                single_index: true,
                ..Default::default()
            },
            |_| Err(tobj::LoadError::GenericFailure),
        )
        .context("Failed to load OBJ")?;

        assert_eq!(models.len(), 1);
        let mesh = &models[0].mesh;

        let vertices: Vec<_> = mesh
            .positions
            .chunks_exact(3)
            .zip(mesh.normals.chunks_exact(3))
            .zip(mesh.texcoords.chunks_exact(2))
            .map(|((pos, norm), uv)| Vertex {
                position: Vec3::new(pos[0], pos[1], pos[2]),
                normal: Vec3::new(norm[0], norm[1], norm[2]),
                uv: Vec2::new(uv[0], uv[1]),
            })
            .collect();
        let vertex_buffer = webgl
            .create_buffer()
            .ok_or_else(|| anyhow!("Failed to create vertex buffer"))?;
        webgl.bind_buffer(WebGlConsts::ARRAY_BUFFER, Some(&vertex_buffer));
        webgl.buffer_data_with_u8_array(
            WebGlConsts::ARRAY_BUFFER,
            bytemuck::cast_slice(&vertices),
            WebGlConsts::STATIC_DRAW,
        );

        let indices: Vec<u16> = mesh
            .indices
            .iter()
            .map(|&idx| idx.try_into().expect("index out of range"))
            .collect();

        let index_buffer = webgl
            .create_buffer()
            .ok_or_else(|| anyhow!("Failed to create index buffer"))?;
        webgl.bind_buffer(WebGlConsts::ELEMENT_ARRAY_BUFFER, Some(&index_buffer));
        webgl.buffer_data_with_u8_array(
            WebGlConsts::ELEMENT_ARRAY_BUFFER,
            bytemuck::cast_slice(&indices),
            WebGlConsts::STATIC_DRAW,
        );

        let texture = webgl
            .create_texture()
            .ok_or_else(|| anyhow!("Failed to create WebGL texture"))?;
        webgl.bind_texture(WebGlConsts::TEXTURE_2D, Some(&texture));

        webgl
            .tex_image_2d_with_i32_and_i32_and_i32_and_format_and_type_and_opt_u8_array(
                WebGlConsts::TEXTURE_2D,
                0,
                WebGlConsts::RGBA as i32,
                1,
                1,
                0,
                WebGlConsts::RGBA,
                WebGlConsts::UNSIGNED_BYTE,
                Some(&[255, 0, 255, 255]),
            )
            .map_err(util::js_value_to_err)
            .context("Failed to upload temp texture data")?;

        let img = HtmlImageElement::new()
            .map_err(util::js_value_to_err)
            .context("Failed to create image element")?;
        img.set_cross_origin(Some(""));
        let img = Rc::new(img);

        let closure = Closure::wrap(Box::new({
            let img = img.clone();
            let webgl = webgl.clone();
            let texture = texture.clone();
            move || {
                webgl.bind_texture(WebGlConsts::TEXTURE_2D, Some(&texture));
                webgl
                    .tex_image_2d_with_u32_and_u32_and_image(
                        WebGlConsts::TEXTURE_2D,
                        0,
                        WebGlConsts::RGBA as i32,
                        WebGlConsts::RGBA,
                        WebGlConsts::UNSIGNED_BYTE,
                        &img,
                    )
                    .map_err(util::js_value_to_err)
                    .expect("Failed to upload texture data");

                if img.width().is_power_of_two() && img.height().is_power_of_two() && mipmapping {
                    webgl.generate_mipmap(WebGlConsts::TEXTURE_2D);

                    webgl.tex_parameteri(
                        WebGlConsts::TEXTURE_2D,
                        WebGlConsts::TEXTURE_MIN_FILTER,
                        WebGlConsts::LINEAR_MIPMAP_LINEAR as _,
                    );
                } else {
                    webgl.tex_parameteri(
                        WebGlConsts::TEXTURE_2D,
                        WebGlConsts::TEXTURE_MIN_FILTER,
                        WebGlConsts::LINEAR as _,
                    );
                }
            }
        }) as Box<dyn FnMut()>);
        img.set_onload(Some(closure.as_ref().unchecked_ref()));
        closure.forget();
        img.set_src(hosted_texture_path);

        Ok(TexturedModel {
            vertex_buffer,
            indices_count: indices.len() as _,
            index_buffer,
            texture,
        })
    }
}

pub struct TexturedPass {
    program: WebGlProgram,
    view_projection_uniform: WebGlUniformLocation,
    sampler_uniform: WebGlUniformLocation,
    instance_buffer: WebGlBuffer,
}

impl TexturedPass {
    pub fn new(webgl: &WebGl) -> Result<TexturedPass> {
        let program = super::compile_program(
            &webgl,
            include_str!("shaders/textured.vs.glsl"),
            include_str!("shaders/textured.fs.glsl"),
        )?;

        let instance_buffer = webgl
            .create_buffer()
            .ok_or_else(|| anyhow!("Failed to create instance buffer"))?;

        let view_projection_uniform = webgl
            .get_uniform_location(&program, "view_projection")
            .ok_or_else(|| anyhow!("Failed to get view_projection uniform location"))?;

        let sampler_uniform = webgl
            .get_uniform_location(&program, "tex")
            .ok_or_else(|| anyhow!("Failed to get sampler2D uniform location"))?;

        Ok(TexturedPass {
            program,
            view_projection_uniform,
            sampler_uniform,
            instance_buffer,
        })
    }

    pub fn draw(
        &mut self,
        app: &mut ApplicationState,
        window_size: PhysicalSize<u32>,
        view_projection: Mat4,
        model: &TexturedModel,
        transforms: &[Mat4],
    ) {
        let instances: Vec<_> = transforms.iter().map(|&model| Instance { model }).collect();

        app.webgl.use_program(Some(&self.program));

        let slot = 0;
        app.webgl.active_texture(WebGlConsts::TEXTURE0 + slot);
        app.webgl
            .bind_texture(WebGlConsts::TEXTURE_2D, Some(&model.texture));
        app.webgl.uniform1i(Some(&self.sampler_uniform), slot as _);

        app.webgl.uniform_matrix4fv_with_f32_array(
            Some(&self.view_projection_uniform),
            false,
            &view_projection.to_cols_array(),
        );

        app.webgl
            .bind_buffer(WebGlConsts::ELEMENT_ARRAY_BUFFER, Some(&model.index_buffer));

        Vertex::apply_vertex_attrib_layout(&app.webgl, &self.program, &model.vertex_buffer);
        Instance::apply_vertex_attrib_layout(
            &app.webgl,
            &self.program,
            &self.instance_buffer,
            &instances,
        );

        app.webgl
            .bind_buffer(WebGlConsts::ARRAY_BUFFER, Some(&self.instance_buffer));
        app.webgl.buffer_data_with_u8_array(
            WebGlConsts::ARRAY_BUFFER,
            bytemuck::cast_slice(&instances),
            WebGlConsts::DYNAMIC_DRAW,
        );

        app.webgl.disable(WebGlConsts::BLEND);
        app.webgl.depth_func(WebGlConsts::GREATER);
        app.webgl.clear_depth(0.0);
        app.webgl.enable(WebGlConsts::DEPTH_TEST);

        app.webgl.cull_face(WebGlConsts::BACK);
        app.webgl.enable(WebGlConsts::CULL_FACE);

        app.webgl
            .viewport(0, 0, window_size.width as _, window_size.height as _);

        app.webgl
            .angle_instanced_arrays
            .draw_elements_instanced_angle_with_i32(
                WebGlConsts::TRIANGLES,
                model.indices_count,
                WebGlConsts::UNSIGNED_SHORT,
                0,
                instances.len() as _,
            );
    }
}
