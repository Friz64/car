uniform mat4 view_projection;

// vertex
attribute vec3 position;
attribute vec3 normal;
attribute vec2 uv;
// instance
attribute mat4 model;

varying highp vec2 frag_uv;
varying vec3 frag_normal;

void main() {
    gl_Position = view_projection * model * vec4(position, 1.0);
    frag_uv = uv * vec2(1.0, -1.0);
    frag_normal = mat3(model) * normal;
}
