uniform mat4 view_projection;

attribute vec3 position;
attribute vec3 color;
attribute mat4 model;

varying vec3 vert_color;

void main() {
    gl_Position = view_projection * model * vec4(position, 1.0);
    vert_color = color;
}
