precision mediump float;

varying highp vec2 frag_uv;
varying vec3 frag_normal;

uniform sampler2D tex;

void main() {
    vec4 color = texture2D(tex, frag_uv);
    float ambient = 0.4;
    float diffuse_irradiance = dot(frag_normal, normalize(vec3(1, 1, 1)));
    gl_FragColor = color * (ambient + max(0.0, min(0.9, diffuse_irradiance)));
}
