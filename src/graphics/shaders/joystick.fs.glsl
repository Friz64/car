precision mediump float;

uniform vec2 dragging;

varying vec2 vert_position;

const float AA_SMOOTH = 0.005;
const float CIRCLE_SIZE = 0.7;
const float CUTOUT_DIST = 0.05;
const float SMOOTH = 0.3;
const float CROSSHAIR_SIZE = 0.02;
const float CROSSHAIR_RADIUS = CIRCLE_SIZE - CUTOUT_DIST;
const float TOTAL_OPACITY = 0.5;

// https://www.iquilezles.org/www/articles/smin/smin.htm
float smin(float a, float b, float k) {
    float h = max(k - abs(a - b), 0.0) / k;
    return min(a, b) - h * h * k * (1.0 / 4.0);
}

void main() {
    vec2 p = vert_position;

    float center_circle = length(p) - CIRCLE_SIZE;
    float dragging_circle_dist = length(p - dragging * CIRCLE_SIZE);
    float dragging_circle = dragging_circle_dist - (1.0 - CIRCLE_SIZE);
    float cutout_circle = dragging_circle_dist - (1.0 - CIRCLE_SIZE - CUTOUT_DIST);
    float circles = max(-cutout_circle, smin(center_circle, dragging_circle, SMOOTH));

    float cross_x = float(abs(p.x) > CROSSHAIR_SIZE || abs(p.y) > CROSSHAIR_RADIUS);
    float cross_y = float(abs(p.y) > CROSSHAIR_SIZE || abs(p.x) > CROSSHAIR_RADIUS);
    float crosshair = min(cross_x, cross_y) - AA_SMOOTH;

    float sdf = max(circles, -crosshair);

    float alpha = smoothstep(-AA_SMOOTH, AA_SMOOTH, -sdf);
    gl_FragColor = vec4(vec3(1.0), alpha * TOTAL_OPACITY);
}
