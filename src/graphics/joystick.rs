use super::WebGlConsts;
use crate::{app::ApplicationState, WebGl};
use anyhow::{anyhow, Result};
use bytemuck::{Pod, Zeroable};
use glam::{const_vec2, Vec2};
use memoffset::offset_of;
use std::{cmp, mem};
use web_sys::{WebGlBuffer, WebGlProgram, WebGlUniformLocation};
use winit::dpi::PhysicalSize;

#[repr(C)]
#[derive(Clone, Copy)]
struct Vertex {
    position: Vec2,
}

const VERTICES: [Vertex; 4] = [
    Vertex {
        position: const_vec2!([1.0, 1.0]),
    },
    Vertex {
        position: const_vec2!([-1.0, 1.0]),
    },
    Vertex {
        position: const_vec2!([1.0, -1.0]),
    },
    Vertex {
        position: const_vec2!([-1.0, -1.0]),
    },
];

const INDICES: [u16; 6] = [0, 1, 2, 3, 2, 1];

unsafe impl Zeroable for Vertex {}
unsafe impl Pod for Vertex {}

impl Vertex {
    const SIZE: usize = mem::size_of::<Self>();

    fn apply_vertex_attrib_layout(
        webgl: &WebGl,
        program: &WebGlProgram,
        vertex_buffer: &WebGlBuffer,
    ) {
        webgl.bind_buffer(WebGlConsts::ARRAY_BUFFER, Some(vertex_buffer));

        let position = webgl.get_attrib_location(&program, "position");
        webgl.enable_vertex_attrib_array(position as _);
        webgl.vertex_attrib_pointer_with_i32(
            position as _,
            2,
            WebGlConsts::FLOAT,
            false,
            Self::SIZE as _,
            offset_of!(Vertex, position) as _,
        );
        webgl
            .angle_instanced_arrays
            .vertex_attrib_divisor_angle(position as _, 0);
    }
}

pub struct JoystickPass {
    program: WebGlProgram,
    dragging_uniform: WebGlUniformLocation,
    vertex_buffer: WebGlBuffer,
    index_buffer: WebGlBuffer,
}

impl JoystickPass {
    pub fn new(webgl: &WebGl) -> Result<JoystickPass> {
        webgl.blend_func(WebGlConsts::SRC_ALPHA, WebGlConsts::ONE_MINUS_SRC_ALPHA);
        webgl.clear_depth(0.0);

        let program = super::compile_program(
            &webgl,
            include_str!("shaders/joystick.vs.glsl"),
            include_str!("shaders/joystick.fs.glsl"),
        )?;

        let vertex_buffer = webgl
            .create_buffer()
            .ok_or_else(|| anyhow!("Failed to create vertex buffer"))?;
        webgl.bind_buffer(WebGlConsts::ARRAY_BUFFER, Some(&vertex_buffer));
        webgl.buffer_data_with_u8_array(
            WebGlConsts::ARRAY_BUFFER,
            bytemuck::cast_slice(&VERTICES),
            WebGlConsts::STATIC_DRAW,
        );

        let index_buffer = webgl
            .create_buffer()
            .ok_or_else(|| anyhow!("Failed to create index buffer"))?;
        webgl.bind_buffer(WebGlConsts::ELEMENT_ARRAY_BUFFER, Some(&index_buffer));
        webgl.buffer_data_with_u8_array(
            WebGlConsts::ELEMENT_ARRAY_BUFFER,
            bytemuck::cast_slice(&INDICES),
            WebGlConsts::STATIC_DRAW,
        );

        let dragging_uniform = webgl
            .get_uniform_location(&program, "dragging")
            .ok_or_else(|| anyhow!("Failed to get dragging uniform location"))?;

        Ok(JoystickPass {
            program,
            dragging_uniform,
            vertex_buffer,
            index_buffer,
        })
    }

    pub fn draw(
        &mut self,
        app: &mut ApplicationState,
        window_size: PhysicalSize<u32>,
        dragging: Vec2,
    ) {
        let adjust = cmp::min(window_size.width, window_size.height) as i32;
        let edge_offset = adjust / 50;
        let size = adjust / 3;

        app.webgl.viewport(
            window_size.width as i32 - edge_offset - size,
            edge_offset,
            size,
            size,
        );
        app.webgl
            .bind_buffer(WebGlConsts::ELEMENT_ARRAY_BUFFER, Some(&self.index_buffer));
        Vertex::apply_vertex_attrib_layout(&app.webgl, &self.program, &self.vertex_buffer);
        app.webgl.use_program(Some(&self.program));
        app.webgl
            .uniform2f(Some(&self.dragging_uniform), dragging.x, -dragging.y);
        app.webgl.enable(WebGlConsts::BLEND);
        app.webgl.draw_elements_with_i32(
            WebGlConsts::TRIANGLES,
            INDICES.len() as _,
            WebGlConsts::UNSIGNED_SHORT,
            0,
        );
    }
}
