use super::WebGlConsts;
use crate::{app::ApplicationState, WebGl};
use anyhow::{anyhow, Result};
use bytemuck::{Pod, Zeroable};
use glam::{const_vec3, Mat4, Vec3, Vec4};
use memoffset::offset_of;
use std::mem;
use web_sys::{WebGlBuffer, WebGlProgram, WebGlUniformLocation};
use winit::dpi::PhysicalSize;

#[repr(C)]
#[derive(Clone, Copy)]
struct Vertex {
    position: Vec3,
    color: Vec3,
}

const VERTICES: [Vertex; 8] = [
    Vertex {
        position: const_vec3!([0.5, 0.5, 0.5]),
        color: const_vec3!([1.0, 1.0, 1.0]),
    },
    Vertex {
        position: const_vec3!([-0.5, 0.5, 0.5]),
        color: const_vec3!([1.0, 0.0, 0.0]),
    },
    Vertex {
        position: const_vec3!([0.5, 0.5, -0.5]),
        color: const_vec3!([0.0, 1.0, 0.0]),
    },
    Vertex {
        position: const_vec3!([-0.5, 0.5, -0.5]),
        color: const_vec3!([0.0, 0.0, 1.0]),
    },
    Vertex {
        position: const_vec3!([0.5, -0.5, 0.5]),
        color: const_vec3!([1.0, 1.0, 0.0]),
    },
    Vertex {
        position: const_vec3!([-0.5, -0.5, 0.5]),
        color: const_vec3!([0.0, 1.0, 1.0]),
    },
    Vertex {
        position: const_vec3!([0.5, -0.5, -0.5]),
        color: const_vec3!([1.0, 0.0, 1.0]),
    },
    Vertex {
        position: const_vec3!([-0.5, -0.5, -0.5]),
        color: const_vec3!([0.0, 0.0, 0.0]),
    },
];

const INDICES: [u16; 36] = [
    2, 1, 0, 1, 2, 3, // top
    4, 5, 6, 7, 6, 5, // bottom
    7, 3, 2, 2, 6, 7, // front
    0, 1, 4, 5, 4, 1, // back
    6, 2, 0, 0, 4, 6, // left
    1, 3, 5, 7, 5, 3, // right
];

unsafe impl Zeroable for Vertex {}
unsafe impl Pod for Vertex {}

impl Vertex {
    const SIZE: usize = mem::size_of::<Self>();

    fn apply_vertex_attrib_layout(
        webgl: &WebGl,
        program: &WebGlProgram,
        vertex_buffer: &WebGlBuffer,
    ) {
        webgl.bind_buffer(WebGlConsts::ARRAY_BUFFER, Some(vertex_buffer));

        let position = webgl.get_attrib_location(&program, "position");
        webgl.enable_vertex_attrib_array(position as _);
        webgl.vertex_attrib_pointer_with_i32(
            position as _,
            3,
            WebGlConsts::FLOAT,
            false,
            Self::SIZE as _,
            offset_of!(Vertex, position) as _,
        );
        webgl
            .angle_instanced_arrays
            .vertex_attrib_divisor_angle(position as _, 0);

        let color = webgl.get_attrib_location(&program, "color");
        webgl.enable_vertex_attrib_array(color as _);
        webgl.vertex_attrib_pointer_with_i32(
            color as _,
            3,
            WebGlConsts::FLOAT,
            false,
            Self::SIZE as _,
            offset_of!(Vertex, color) as _,
        );
        webgl
            .angle_instanced_arrays
            .vertex_attrib_divisor_angle(color as _, 0);
    }
}

#[repr(C)]
#[derive(Clone, Copy)]
struct Instance {
    model: Mat4,
}

unsafe impl Zeroable for Instance {}
unsafe impl Pod for Instance {}

impl Instance {
    const SIZE: usize = mem::size_of::<Self>();

    fn apply_vertex_attrib_layout(
        webgl: &WebGl,
        program: &WebGlProgram,
        instance_buffer: &WebGlBuffer,
    ) {
        webgl.bind_buffer(WebGlConsts::ARRAY_BUFFER, Some(instance_buffer));

        let model = webgl.get_attrib_location(&program, "model");
        for i in 0..4 {
            let model = model + i;
            webgl.enable_vertex_attrib_array(model as _);
            let offset = mem::size_of::<Vec4>() as i32 * i;
            webgl.vertex_attrib_pointer_with_i32(
                model as _,
                4,
                WebGlConsts::FLOAT,
                false,
                Self::SIZE as _,
                offset_of!(Instance, model) as i32 + offset,
            );

            webgl
                .angle_instanced_arrays
                .vertex_attrib_divisor_angle(model as _, 1);
        }
    }
}

pub struct CubePass {
    program: WebGlProgram,
    view_projection_uniform: WebGlUniformLocation,
    vertex_buffer: WebGlBuffer,
    instance_buffer: WebGlBuffer,
    index_buffer: WebGlBuffer,
    frame_instances: Vec<Instance>,
}

impl CubePass {
    pub fn new(webgl: &WebGl) -> Result<CubePass> {
        webgl.depth_func(WebGlConsts::GREATER);
        webgl.clear_depth(0.0);
        webgl.enable(WebGlConsts::DEPTH_TEST);

        webgl.cull_face(WebGlConsts::BACK);
        webgl.enable(WebGlConsts::CULL_FACE);

        let program = super::compile_program(
            &webgl,
            include_str!("shaders/cube.vs.glsl"),
            include_str!("shaders/cube.fs.glsl"),
        )?;

        let vertex_buffer = webgl
            .create_buffer()
            .ok_or_else(|| anyhow!("Failed to create vertex buffer"))?;
        webgl.bind_buffer(WebGlConsts::ARRAY_BUFFER, Some(&vertex_buffer));
        webgl.buffer_data_with_u8_array(
            WebGlConsts::ARRAY_BUFFER,
            bytemuck::cast_slice(&VERTICES),
            WebGlConsts::STATIC_DRAW,
        );

        let instance_buffer = webgl
            .create_buffer()
            .ok_or_else(|| anyhow!("Failed to create instance buffer"))?;

        let index_buffer = webgl
            .create_buffer()
            .ok_or_else(|| anyhow!("Failed to create index buffer"))?;
        webgl.bind_buffer(WebGlConsts::ELEMENT_ARRAY_BUFFER, Some(&index_buffer));
        webgl.buffer_data_with_u8_array(
            WebGlConsts::ELEMENT_ARRAY_BUFFER,
            bytemuck::cast_slice(&INDICES),
            WebGlConsts::STATIC_DRAW,
        );

        let view_projection_uniform = webgl
            .get_uniform_location(&program, "view_projection")
            .ok_or_else(|| anyhow!("Failed to get view_projection uniform location"))?;

        Ok(CubePass {
            program,
            view_projection_uniform,
            vertex_buffer,
            instance_buffer,
            index_buffer,
            frame_instances: Vec::new(),
        })
    }

    pub fn add_instance(&mut self, model: Mat4) {
        self.frame_instances.push(Instance { model });
    }

    pub fn draw(
        &mut self,
        app: &mut ApplicationState,
        window_size: PhysicalSize<u32>,
        view_projection: Mat4,
    ) {
        app.webgl
            .viewport(0, 0, window_size.width as _, window_size.height as _);
        app.webgl
            .bind_buffer(WebGlConsts::ELEMENT_ARRAY_BUFFER, Some(&self.index_buffer));
        Vertex::apply_vertex_attrib_layout(&app.webgl, &self.program, &self.vertex_buffer);
        Instance::apply_vertex_attrib_layout(&app.webgl, &self.program, &self.instance_buffer);
        app.webgl
            .bind_buffer(WebGlConsts::ARRAY_BUFFER, Some(&self.instance_buffer));
        app.webgl.buffer_data_with_u8_array(
            WebGlConsts::ARRAY_BUFFER,
            bytemuck::cast_slice(&self.frame_instances),
            WebGlConsts::DYNAMIC_DRAW,
        );

        app.webgl.use_program(Some(&self.program));
        app.webgl.uniform_matrix4fv_with_f32_array(
            Some(&self.view_projection_uniform),
            false,
            &view_projection.to_cols_array(),
        );
        app.webgl.disable(WebGlConsts::BLEND);
        app.webgl
            .angle_instanced_arrays
            .draw_elements_instanced_angle_with_i32(
                WebGlConsts::TRIANGLES,
                INDICES.len() as _,
                WebGlConsts::UNSIGNED_SHORT,
                0,
                self.frame_instances.len() as _,
            );

        self.frame_instances.clear();
    }
}
