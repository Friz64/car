mod car;
mod cube;
mod portal;

use self::{
    car::CarObject,
    cube::{CubeObject, CubeType},
    portal::PortalObject,
};
use crate::{
    app::ApplicationState,
    graphics::{cube::CubePass, textured::TexturedPass, Models},
    physics::Physics,
};
use glam::{EulerRot, Mat4, Quat, Vec3};
pub use portal::PortalType;
use rapier3d::{
    dynamics::{RigidBody, RigidBodyHandle},
    geometry::{Collider, ColliderHandle},
    prelude::ColliderBuilder,
};
use slab::Slab;
use winit::dpi::PhysicalSize;

const COLLISION_GROUP_CAR_CHASSIS: u32 = 1 << 0;
const COLLISION_GROUP_CAR_WHEEL: u32 = 1 << 1;

pub struct Objects {
    pub slab: Slab<Object>,
    pub kerrek: usize,
    pub twitter_portal: usize,
    pub blog_portal: usize,
}

impl Objects {
    pub fn new(physics: &mut Physics) -> Objects {
        let mut slab = Slab::new();
        for y in 0..20 {
            slab.insert(Object::new_cube(
                physics,
                Vec3::new(0.0, 10.0 + 1.1 * y as f32, -10.0),
                Quat::from_rotation_y(y as f32),
                Vec3::ONE,
                CubeType::Dynamic { density: 500.0 },
            ));
        }

        slab.insert(Object::new_cube(
            physics,
            Vec3::new(0.0, -12.0, 100.0),
            Quat::from_rotation_x(-20.0f32.to_radians()),
            Vec3::ONE * 10.0,
            CubeType::Static,
        ));

        let suspension_course_pos = Vec3::new(-15.0, -9.00, 50.0);
        for i in 0..100 {
            let i = i as f32;
            let offset = i * (1.0 + i * 0.01);

            slab.insert(Object::new_cube(
                physics,
                suspension_course_pos + Vec3::Z * offset,
                Quat::IDENTITY,
                Vec3::new(10.0, 0.5, 0.5),
                CubeType::Static,
            ));

            slab.insert(Object::new_cube(
                physics,
                suspension_course_pos + Vec3::Z * offset + Vec3::Y * 10.0 - Vec3::X * 4.0,
                Quat::IDENTITY,
                Vec3::new(0.5, 0.5, 0.5),
                CubeType::Dynamic { density: 800.0 },
            ));
        }

        let floor_size = Vec3::ONE * 50.0;
        for x in -20..20 {
            for z in -20..20 {
                slab.insert(Object::new_cube(
                    physics,
                    floor_size * Vec3::new(x as f32, 0.0, z as f32)
                        + Vec3::new(0.0, -10.0 - 24.0, 0.0),
                    Quat::IDENTITY,
                    floor_size,
                    CubeType::Static,
                ));
            }
        }

        let kerrek = slab.insert(Object::new_car(
            physics,
            Vec3::new(0.0, -8.0, 0.0),
            Quat::from_euler(
                EulerRot::XYZ,
                10.0f32.to_radians(),
                0.0f32.to_radians(),
                10.0f32.to_radians(),
            ),
            Vec3::ZERO,
            Vec3::ZERO,
        ));

        let twitter_portal = slab.insert(Object::new_portal(
            PortalType::Twitter,
            physics,
            Vec3::new(-27.0, 2.0, -27.0),
            Quat::from_rotation_x(90.0f32.to_radians())
                * Quat::from_rotation_z(-40.0f32.to_radians()),
            ColliderBuilder::cylinder(1.0, 11.2418).build(),
        ));

        let blog_portal = slab.insert(Object::new_portal(
            PortalType::Blog,
            physics,
            Vec3::new(-10.0, -0.9, -40.0),
            Quat::from_rotation_x(90.0f32.to_radians())
                * Quat::from_rotation_z(-25.0f32.to_radians()),
            ColliderBuilder::cylinder(1.0, 8.18).build(),
        ));

        Objects {
            slab,
            kerrek,
            twitter_portal,
            blog_portal,
        }
    }
}

pub struct ObjectPartRenderInfo {
    // origin of graphical model
    pub translation: Vec3,
    pub rotation: Quat,
    pub scale: Vec3,
}

impl ObjectPartRenderInfo {
    pub fn new(translation: Vec3, rotation: Quat, scale: Vec3) -> ObjectPartRenderInfo {
        ObjectPartRenderInfo {
            translation,
            rotation,
            scale,
        }
    }

    fn update(&mut self, translation: Vec3, rotation: Quat) {
        self.translation = translation;
        self.rotation = rotation;
    }

    pub fn model_mat(&self) -> Mat4 {
        Mat4::from_scale_rotation_translation(self.scale, self.rotation, self.translation)
    }
}

pub struct ObjectPartPhysics {
    pub rigidbody_handle: RigidBodyHandle,
    pub collider_handle: Option<ColliderHandle>,
}

impl ObjectPartPhysics {
    pub fn new(
        physics: &mut Physics,
        rigidbody: RigidBody,
        collider: Option<Collider>,
    ) -> ObjectPartPhysics {
        let rigidbody_handle = physics.bodies.insert(rigidbody);
        let collider_handle = collider.map(|collider| {
            physics
                .colliders
                .insert_with_parent(collider, rigidbody_handle, &mut physics.bodies)
        });

        ObjectPartPhysics {
            rigidbody_handle,
            collider_handle,
        }
    }

    pub fn translation_rotation(&self, physics: &Physics) -> (Vec3, Quat) {
        let rigidbody = physics
            .bodies
            .get(self.rigidbody_handle)
            .expect("Rigid body of ObjectPartPhysics doesn't exist anymore");

        let position = rigidbody.position();
        (
            Vec3::from(position.translation),
            Quat::from(position.rotation),
        )
    }

    /*
    pub fn velocities(&self, physics: &Physics) -> (Vec3, Vec3) {
        let rigidbody = physics
            .bodies
            .get(self.rigidbody_handle)
            .expect("Rigid body of ObjectPartPhysics doesn't exist anymore");

        let rb_linvel = rigidbody.linvel();
        let linvel = Vec3::new(rb_linvel.x, rb_linvel.y, rb_linvel.z);
        let rb_angvel = rigidbody.angvel();
        let angvel = Vec3::new(rb_angvel.x, rb_angvel.y, rb_angvel.z);
        (linvel, angvel)
    }*/
}

pub struct ObjectPart {
    pub render_info: Option<ObjectPartRenderInfo>,
    pub physics_component: Option<ObjectPartPhysics>,
}

impl ObjectPart {
    fn new(
        render_info: Option<ObjectPartRenderInfo>,
        physics_component: Option<ObjectPartPhysics>,
    ) -> ObjectPart {
        ObjectPart {
            render_info,
            physics_component,
        }
    }

    fn update(&mut self, physics: &mut Physics) {
        if let (Some(render_info), Some(physics_component)) =
            (&mut self.render_info, &self.physics_component)
        {
            let (translation, rotation) = physics_component.translation_rotation(physics);
            render_info.update(translation, rotation);
        }
    }

    pub fn render_info(&self) -> &ObjectPartRenderInfo {
        self.render_info.as_ref().expect("render_info not present")
    }

    pub fn physics_component(&self) -> &ObjectPartPhysics {
        self.physics_component
            .as_ref()
            .expect("physics_component not present")
    }
}

pub enum Object {
    Cube(CubeObject),
    Car(CarObject),
    Portal(PortalObject),
}

impl Object {
    pub fn draw(
        &mut self,
        app: &mut ApplicationState,
        window_size: PhysicalSize<u32>,
        view_projection: Mat4,
        models: &Models,
        physics: &mut Physics,
        cube_pass: &mut CubePass,
        textured_pass: &mut TexturedPass,
    ) {
        match self {
            Object::Cube(inner) => inner.draw(physics, cube_pass),
            Object::Car(inner) => inner.draw(
                app,
                physics,
                textured_pass,
                window_size,
                view_projection,
                models,
            ),
            Object::Portal(inner) => inner.draw(
                app,
                physics,
                textured_pass,
                window_size,
                view_projection,
                models,
            ),
        }
    }
}
