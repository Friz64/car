use super::{
    Object, ObjectPart, ObjectPartPhysics, ObjectPartRenderInfo, COLLISION_GROUP_CAR_CHASSIS,
    COLLISION_GROUP_CAR_WHEEL,
};
use crate::physics::GRAVITY;
use crate::powertrain::{clamp_abs, CarPowertrain, Clutch, Engine, Fueltank, Gearbox};
use crate::{
    app::ApplicationState,
    graphics::{textured::TexturedPass, Models},
    physics::Physics,
};
use glam::{const_vec2, const_vec3, Mat4, Quat, Vec2, Vec3};
use nalgebra::{Isometry, Isometry3, Point3, UnitQuaternion, Vector3};
use rapier3d::{
    dynamics::{MassProperties, RigidBodyBuilder, RigidBodyType},
    geometry::{ColliderBuilder, ConvexPolyhedron, Cylinder, InteractionGroups, SharedShape},
};
use rapier3d::{parry::query::TOIStatus, prelude::ActiveEvents};
use std::f32::consts::{FRAC_PI_2, TAU};
use std::rc::Rc;
use winit::dpi::PhysicalSize;

const CHASSIS_CONVEX_HULL: [[f32; 3]; 22] = [
    [-1.056160, 0.030193, 1.928491],
    [-0.998811, 0.751073, 1.828390],
    [-1.034971, 0.038209, -1.718789],
    [-0.727438, 0.981081, -2.419853],
    [1.056160, 0.030193, 1.928491],
    [0.998811, 0.751073, 1.828390],
    [1.034971, 0.038209, -1.718789],
    [0.727438, 0.981081, -2.419853],
    [-0.790706, 0.030193, 2.451529],
    [-0.727554, 0.588542, 2.323946],
    [0.790706, 0.030193, 2.451529],
    [0.727554, 0.588542, 2.323946],
    [-0.745017, 1.409012, -1.047388],
    [0.745017, 1.409012, -1.047388],
    [-0.745017, 1.392300, 0.260566],
    [0.745017, 1.392300, 0.260566],
    [-0.829896, 0.444083, -2.359989],
    [0.829896, 0.444083, -2.359989],
    [1.027850, 0.966869, -1.805947],
    [-1.027850, 0.966869, -1.805947],
    [0.000000, 0.030193, 2.579086],
    [0.000000, 0.588542, 2.425835],
];

const CHASSIS_CUBOID_SIZE: Vec3 = const_vec3!([2.08, 1.41, 4.95]);
/// height of chassis part above ground
const CHASSIS_HEIGHT: f32 = 0.26;
const CHASSIS_LOCAL_COM: Vec3 = const_vec3!([0.0, 0.27, 0.35]);
/// Vec2(offset to the left, offset forwards)
const WHEEL_FRONT_OFFSET: Vec2 = const_vec2!([0.935, 1.48]);
const WHEEL_REAR_OFFSET: Vec2 = const_vec2!([0.935, -1.27]);
/// wheel hub height above car bottom, adds margin to prevent raycast toi of zero
const WHEEL_HUB_HEIGHT: f32 = SUSPENSION_TRAVEL + WHEEL_RADIUS - CHASSIS_HEIGHT - 0.1;
const WHEEL_DEPTH: f32 = 0.264;
const WHEEL_RADIUS: f32 = 0.785 / 2.0;
const WHEEL_MODEL_AXLE_AXIS: Vec3 = Vec3::Y;
const CHASSIS_MASS: f32 = 2000.0;
const SUSPENSION_TRAVEL: f32 = 0.5;

#[derive(Copy, Clone)]
pub struct TireModel {
    /// A tire has less friction if it is not vertical to ground normal.
    /// At `angle=acos(tilt_start)` the tire loses grip linearly with relative ground normal up to `angle=acos(tilt_end)`
    pub tilt_start: f32,
    pub tilt_end: f32,
    /// At which velocity in m/s after `slip_static_friction_threshold` traction is reduced by half.
    /// Higher values result in a more gradual loss of grip.
    pub slip_half_traction: f32,
    /// At which velocity in m/s friction begins dropping exponentially for every N of weight on the tire.
    /// This is called traction for Coulomb friction.
    /// Unit: m/s per N = Nm/s, velocity for given force
    pub slip_static_friction_threshold: f32,
    /// Higher values amplify the effect of relative lateral tire motion on slip. Skews the meaning of other slip factors.
    pub slip_lateral_fac: f32,
    /// Higher values amplify the effect of relative longitudinal tire motion on slip. Skews the meaning of other slip factors.
    pub slip_longitudinal_fac: f32,
    pub stiction_coeff: f32,
    pub friction_coeff: f32,
}

impl Default for TireModel {
    fn default() -> Self {
        let static_friction_loss_velocity = 3.0;

        Self {
            tilt_start: 20f32.to_radians().cos(),
            tilt_end: 45f32.to_radians().cos(),
            // very forgiving values lol
            slip_half_traction: 3.0,
            slip_static_friction_threshold: static_friction_loss_velocity
                / (CHASSIS_MASS * GRAVITY * 0.25),
            slip_lateral_fac: 1.7,
            slip_longitudinal_fac: 1.0,
            stiction_coeff: 1.0,
            // https://en.wikipedia.org/wiki/Friction#Approximate_coefficients_of_friction
            friction_coeff: 0.45,
        }
    }
}

impl TireModel {
    pub fn tilt_coeff(&self, tire_tilt: f32) -> f32 {
        1.0 - ((tire_tilt - 1.0 + self.tilt_start) / (self.tilt_start - self.tilt_end))
            .max(0.0)
            .min(1.0)
    }

    pub fn slip_coeff(
        &self,
        longitudinal_velocity: f32,
        lateral_velocity: f32,
        weight: f32,
    ) -> f32 {
        let slip_vel = ((longitudinal_velocity * self.slip_longitudinal_fac).powi(2)
            + (lateral_velocity * self.slip_lateral_fac).powi(2))
        .sqrt();

        let friction_coeff = if slip_vel < self.slip_static_friction_threshold * weight {
            self.stiction_coeff
        } else {
            self.friction_coeff
        };

        friction_coeff
    }
}

pub struct Wheel {
    pub object_part: ObjectPart,
    /// Suspension strut hard point in local car space.
    pub hub_position: Vec3,
    /// ray direction (downwards) in local car space, normalized
    pub travel_direction: Vec3,
    /// Normalized outward axle direction in local car space
    pub axle_direction: Vec3,
    /// position in meters along `travel_direction`
    pub position: f32,
    pub last_position: f32,
    /// maximum travel
    pub travel: f32,
    /// in N/m
    pub spring_coeff: f32,
    /// last spring force in N
    pub spring_force: f32,
    /// used to modify spring in N
    pub spring_additional_force: f32,
    /// in N/(m/s) = N*s/m
    pub damping_compression: f32,
    /// in N/(m/s) = N*s/m
    pub damping_relaxation: f32,
    /// in N
    pub max_damping: f32,
    /// when the rubber gives up in N
    pub max_horizontal_force: f32,
    pub depth: f32,
    pub radius: f32,
    /// powered
    pub driven: bool,
    /// is the wheel located on the right side in direction of travel
    pub is_right_side: bool,
    /// angular velocity in rad/s
    pub angular_velocity: f32,
    /// displayed angle of wheel in radians
    pub angle: f32,
    pub angular_inertia: f32,
    /// motor torque in Nm
    pub motor: f32,
    pub tire_model: Rc<TireModel>,
    pub is_grounded: bool,
}

impl Wheel {
    pub fn new(
        physics: &mut Physics,
        chassis: &ObjectPart,
        horizontal_offset: Vec2,
        is_right_side: bool,
        driven: bool,
        tire_model: Rc<TireModel>,
    ) -> Wheel {
        let hub_position = Vec3::new(
            horizontal_offset.x * if is_right_side { -1.0 } else { 1.0 },
            WHEEL_HUB_HEIGHT,
            horizontal_offset.y,
        );

        let mut wheel = Wheel {
            object_part: ObjectPart::new(
                Some(ObjectPartRenderInfo::new(
                    Vec3::ZERO,
                    Quat::IDENTITY,
                    Vec3::ONE,
                )),
                None,
            ),
            hub_position,
            travel_direction: -Vec3::Y,
            axle_direction: if is_right_side { Vec3::X } else { -Vec3::X },
            position: SUSPENSION_TRAVEL,
            last_position: SUSPENSION_TRAVEL,
            travel: SUSPENSION_TRAVEL,
            spring_coeff: CHASSIS_MASS * GRAVITY * 0.25 / 0.11 * 1.3,
            spring_force: 0.0,
            spring_additional_force: 0.0,
            damping_compression: CHASSIS_MASS * 8.0,
            damping_relaxation: CHASSIS_MASS * 3.0,
            max_damping: CHASSIS_MASS * 3.0,
            max_horizontal_force: CHASSIS_MASS * 14.0,
            depth: WHEEL_DEPTH,
            radius: WHEEL_RADIUS,
            driven,
            is_right_side,
            angular_velocity: 0.0,
            angle: 0.0,
            angular_inertia: 0.5 * 17.0 * WHEEL_RADIUS.powi(2),
            motor: 0.0,
            tire_model,
            is_grounded: false,
        };

        wheel.update_render_info(physics, chassis);
        wheel
    }

    pub fn post_physics_step(&mut self, dt: f32, physics: &mut Physics, chassis: &ObjectPart) {
        let chassis_body = &physics.bodies[chassis.physics_component().rigidbody_handle];

        let chassis_rotation = Quat::from(*chassis_body.rotation());

        let shapecast_pos = chassis_body.position()
            * Isometry3::from_parts(
                self.hub_position.into(),
                UnitQuaternion::from_scaled_axis(Vector3::from(
                    // rapier cylinder has depth along Y
                    self.axle_direction.cross(Vec3::Y) * FRAC_PI_2,
                )),
            );

        let shapecast_vel = chassis_body.position().rotation * Vector3::from(self.travel_direction);
        let shape = Cylinder {
            half_height: self.depth * 0.5,
            radius: self.radius,
        };

        let cast = physics.query_pipeline.cast_shape(
            &physics.colliders,
            &shapecast_pos,
            &shapecast_vel,
            &shape,
            self.travel,
            InteractionGroups::new(COLLISION_GROUP_CAR_WHEEL, !COLLISION_GROUP_CAR_CHASSIS),
            None,
        );

        fn project_onto_plane(plane_normal: Vec3, v: Vec3) -> Vec3 {
            v - plane_normal * v.dot(plane_normal)
        }

        self.angular_velocity += self.motor / self.angular_inertia * dt;

        let mut clamped_arresting_torque = 0.0;

        self.last_position = self.position;

        match cast {
            Some((other_collider_handle, toi)) if toi.status == TOIStatus::Converged => {
                self.is_grounded = true;
                self.position = toi.toi;

                let wheel_contact_point = shapecast_pos * toi.witness2 + shapecast_vel * toi.toi;

                let wheel_contact_point_velocity =
                    Vec3::from(chassis_body.velocity_at_point(&wheel_contact_point));

                let ground_collider = &physics.colliders[other_collider_handle];

                let ground_body = &physics.bodies[ground_collider.parent().unwrap()];

                // https://github.com/dimforge/rapier/pull/234
                let ground_contact_point = toi.witness1;

                let ground_body_contact_point_velocity =
                    Vec3::from(ground_body.velocity_at_point(&ground_contact_point));

                // https://github.com/dimforge/rapier/pull/234
                let ground_normal = Vec3::from(*toi.normal1);

                let forwards = ground_normal
                    .cross(chassis_rotation * self.axle_direction)
                    .normalize_or_zero();

                // velocity relative to ground
                let axle_velocity =
                    wheel_contact_point_velocity - ground_body_contact_point_velocity;

                let contact_patch_velocity =
                    axle_velocity - forwards * (self.radius * self.angular_velocity);
                let horizontal_relative_velocity =
                    project_onto_plane(ground_normal, contact_patch_velocity);

                // :(
                let depression = self.travel - self.position;

                //let susp_vel = (self.position - self.last_position) / dt;
                let susp_vel = ground_normal.dot(axle_velocity)
                    / -ground_normal.dot(chassis_rotation * self.travel_direction);

                let damping = clamp_abs(
                    if susp_vel < 0.0 {
                        susp_vel * self.damping_compression
                    } else {
                        susp_vel * self.damping_relaxation
                    },
                    self.max_damping,
                );

                self.spring_force = depression * self.spring_coeff - damping;

                let spring_force = (self.spring_additional_force + self.spring_force).max(0.0);

                self.spring_additional_force = 0.0;

                let normal_force = spring_force;

                //let normal_force = CHASSIS_MASS * GRAVITY * 0.25;

                let longitudinal_velocity = horizontal_relative_velocity.dot(forwards);
                let lateral_velocity =
                    horizontal_relative_velocity.dot(chassis_rotation * self.axle_direction);

                // calculate friction coefficient for tire slip
                let slip_coeff = self.tire_model.slip_coeff(
                    longitudinal_velocity,
                    lateral_velocity,
                    normal_force,
                );

                let upness = -ground_normal
                    .dot(chassis_rotation * self.travel_direction)
                    .min(0.0);

                // lateral contact patch force
                let mut horizontal_forces =
                    horizontal_relative_velocity * normal_force * slip_coeff;

                if horizontal_forces.length_squared() > self.max_horizontal_force.powi(2) {
                    horizontal_forces *= self.max_horizontal_force / horizontal_forces.length()
                }

                let tilt_coeff = self.tire_model.tilt_coeff(
                    ground_normal
                        .dot(chassis_rotation * self.axle_direction)
                        .abs(),
                );

                // final wheel force
                // tilt is applied after limiting
                let applied_force =
                    (spring_force * ground_normal - horizontal_forces * tilt_coeff) * upness;

                let reaction_torque = forwards.dot(applied_force) * self.radius;

                // the arresting force acts to stop relative longitudinal motion of the wheel to the ground.
                // due to finite time steps this torque can overshoot, thus we clamp it to the torque needed to arrest
                // relative longitudinal wheel motion on ground.
                clamped_arresting_torque = clamp_abs(
                    reaction_torque,
                    longitudinal_velocity.abs() * self.angular_inertia / self.radius / dt,
                );

                if let Some((other_collider_handle, _toi)) = cast {
                    let ground_body = &mut physics.bodies
                        [physics.colliders[other_collider_handle].parent().unwrap()];

                    let ground_body_mass = if ground_body.is_dynamic() {
                        let ground_body_mass = ground_body.mass();
                        /*let mass_ratio = (CHASSIS_MASS / (CHASSIS_MASS + ground_body_mass))
                                                    .min(ground_body_mass / CHASSIS_MASS);
                        */
                        let mass_ratio = CHASSIS_MASS / (CHASSIS_MASS + ground_body_mass);

                        // XXX: Workaround https://github.com/dimforge/rapier/issues/216
                        ground_body.lock_rotations(false, true);

                        ground_body.apply_force_at_point(
                            (-applied_force * mass_ratio).into(),
                            ground_contact_point,
                            true,
                        );

                        Some(ground_body_mass)
                    } else {
                        None
                    };

                    let chassis_body =
                        &mut physics.bodies[chassis.physics_component().rigidbody_handle];
                    let mass_ratio = if let Some(ground_body_mass) = ground_body_mass {
                        ground_body_mass / (ground_body_mass + CHASSIS_MASS)
                    } else {
                        1.0
                    };

                    // XXX: Workaround https://github.com/dimforge/rapier/issues/216
                    chassis_body.lock_rotations(false, true);

                    chassis_body.apply_force_at_point(
                        (applied_force * mass_ratio).into(),
                        wheel_contact_point,
                        true,
                    );
                    /*chassis_body.apply_impulse_at_point(
                        (applied_force * mass_ratio * dt).into(),
                        wheel_contact_point,
                        false,
                    );*/
                }
            }
            _ => {
                self.is_grounded = false;
                self.position = self.travel;
            }
        }

        // coupling of force back into wheel
        self.angular_velocity -= clamped_arresting_torque / self.angular_inertia * dt;

        // step wheel
        self.angle = (self.angle + self.angular_velocity * dt).rem_euclid(TAU);
    }

    pub fn side_factor(&self) -> f32 {
        if self.is_right_side {
            -1.0
        } else {
            1.0
        }
    }

    fn update_render_info(&mut self, physics: &mut Physics, chassis: &ObjectPart) {
        let render_info = self.object_part.render_info.as_mut().unwrap();
        let (chassis_translation, chassis_rotation) =
            chassis.physics_component().translation_rotation(physics);

        render_info.translation = chassis_translation
            + chassis_rotation * (self.hub_position + self.position * self.travel_direction);

        let wheel_model_rotation = self.axle_direction.cross(WHEEL_MODEL_AXLE_AXIS);
        render_info.rotation = chassis_rotation
            * Quat::from_scaled_axis(self.axle_direction * -self.angle)
            * Quat::from_scaled_axis(wheel_model_rotation * FRAC_PI_2);
    }
}

pub struct CarObject {
    pub chassis: ObjectPart,
    pub wheel_fl: Wheel,
    pub wheel_fr: Wheel,
    pub wheel_rl: Wheel,
    pub wheel_rr: Wheel,
    pub powertrain: CarPowertrain,
    pub brake_input: f32,
    pub shift_cooldown: f32,
    pub automatic_controls: bool,
    pub velocity: Vec3,
    pub acceleration: Vec3,
}

impl CarObject {
    pub fn draw(
        &mut self,
        app: &mut ApplicationState,
        physics: &mut Physics,
        textured_pass: &mut TexturedPass,
        window_size: PhysicalSize<u32>,
        view_projection: Mat4,
        models: &Models,
    ) {
        self.chassis.update(physics);
        textured_pass.draw(
            app,
            window_size,
            view_projection,
            &models.car,
            &[self.chassis.render_info().model_mat()],
        );

        self.wheel_fl.update_render_info(physics, &self.chassis);
        self.wheel_fr.update_render_info(physics, &self.chassis);
        self.wheel_rl.update_render_info(physics, &self.chassis);
        self.wheel_rr.update_render_info(physics, &self.chassis);
        textured_pass.draw(
            app,
            window_size,
            view_projection,
            &models.wheel,
            &[
                self.wheel_fl.object_part.render_info().model_mat(),
                self.wheel_fr.object_part.render_info().model_mat(),
                self.wheel_rl.object_part.render_info().model_mat(),
                self.wheel_rr.object_part.render_info().model_mat(),
            ],
        );
    }

    pub fn post_physics_step(&mut self, dt: f32, physics: &mut Physics) {
        let all_wheels = [
            &self.wheel_fl,
            &self.wheel_fr,
            &self.wheel_rl,
            &self.wheel_rr,
        ];

        let driven_wheels = all_wheels
            .iter()
            .filter(|w| w.driven)
            .map(|w| *w)
            .collect::<Vec<_>>();
        let driven_wheels_count = driven_wheels.len();

        // differential gearbox splits torque
        let wheel_torque = self.powertrain.update(
            dt,
            &driven_wheels
                .iter()
                .map(|w| w.side_factor() * w.angular_velocity)
                .collect::<Vec<f32>>(),
            driven_wheels.iter().map(|w| w.angular_inertia).sum::<f32>(),
        ) / driven_wheels_count as f32;

        let all_wheels_mut = [
            &mut self.wheel_fl,
            &mut self.wheel_fr,
            &mut self.wheel_rl,
            &mut self.wheel_rr,
        ];

        for wheel in all_wheels_mut {
            wheel.motor = 0.0;

            if wheel.driven {
                wheel.motor += wheel_torque * wheel.side_factor();
            }

            wheel.motor += clamp_abs(
                self.brake_input * -wheel.angular_velocity.signum(),
                wheel.angular_velocity.abs() * wheel.angular_inertia / dt,
            );
        }

        let antiroll = 0.0;

        let f_antiroll = self.wheel_fr.spring_force - self.wheel_fl.spring_force;
        self.wheel_fr.spring_additional_force += antiroll * f_antiroll;
        self.wheel_fl.spring_additional_force -= antiroll * f_antiroll;
        self.wheel_fl.post_physics_step(dt, physics, &self.chassis);
        self.wheel_fr.post_physics_step(dt, physics, &self.chassis);

        let r_antiroll = self.wheel_rl.spring_force - self.wheel_rr.spring_force;
        self.wheel_rr.spring_additional_force += antiroll * r_antiroll;
        self.wheel_rl.spring_additional_force -= antiroll * r_antiroll;
        self.wheel_rl.post_physics_step(dt, physics, &self.chassis);
        self.wheel_rr.post_physics_step(dt, physics, &self.chassis);

        let car_body = &mut physics.bodies[self
            .chassis
            .physics_component
            .as_mut()
            .unwrap()
            .rigidbody_handle];
        let velocity: Vec3 = (*car_body.linvel()).into();
        let acceleration = (velocity - self.velocity) / dt;
        self.velocity = velocity;
        self.acceleration = acceleration;
    }

    pub fn avg_wheel_speed(&self) -> f32 {
        3.6 * 0.25
            * (self.wheel_fl.angular_velocity * self.wheel_fl.radius * self.wheel_fl.side_factor()
                + self.wheel_fr.angular_velocity
                    * self.wheel_fr.radius
                    * self.wheel_fr.side_factor()
                + self.wheel_rl.angular_velocity
                    * self.wheel_rl.radius
                    * self.wheel_rl.side_factor()
                + self.wheel_rr.angular_velocity
                    * self.wheel_rr.radius
                    * self.wheel_rr.side_factor())
    }
}

impl Object {
    pub fn new_car(
        physics: &mut Physics,
        translation: Vec3,
        rotation: Quat,
        velocity: Vec3,
        angular_velocity: Vec3,
    ) -> Object {
        let chassis_translation = translation + rotation * Vec3::new(0.0, CHASSIS_HEIGHT, 0.0);
        let chassis_rigidbody = RigidBodyBuilder::new(RigidBodyType::Dynamic)
            .position(Isometry::from_parts(
                Point3::from(chassis_translation).into(),
                UnitQuaternion::from_quaternion(rotation.into()),
            ))
            .linvel(velocity.into())
            .angvel(angular_velocity.into())
            .additional_mass_properties({
                let mut mass_properties =
                    MassProperties::from_cuboid(1.0, (CHASSIS_CUBOID_SIZE * 0.5).into());

                mass_properties.set_mass(CHASSIS_MASS, true);
                MassProperties {
                    local_com: CHASSIS_LOCAL_COM.into(),
                    ..mass_properties
                }
            })
            .can_sleep(false)
            .build();

        let chassis_collider = {
            let points: Vec<_> = CHASSIS_CONVEX_HULL.iter().map(|&xyz| xyz.into()).collect();
            let shape = ConvexPolyhedron::from_convex_hull(&points)
                .expect("Convex hull computation failed");

            ColliderBuilder::new(SharedShape::new(shape))
                .mass_properties(nalgebra::zero())
                .collision_groups(InteractionGroups::new(
                    COLLISION_GROUP_CAR_CHASSIS,
                    !COLLISION_GROUP_CAR_WHEEL,
                ))
                .active_events(ActiveEvents::CONTACT_EVENTS)
                .build()
        };

        let chassis = ObjectPart::new(
            Some(ObjectPartRenderInfo::new(translation, rotation, Vec3::ONE)),
            Some(ObjectPartPhysics::new(
                physics,
                chassis_rigidbody,
                Some(chassis_collider),
            )),
        );

        fn r2v(rpm: f32) -> f32 {
            rpm / 60.0 * TAU
        }

        let engine_torque_curve = vec![
            0.09, 0.12, 0.16, 0.25, 0.32, 0.45, 0.58, 0.73, 0.88, 0.93, 1.0, 1.0, 0.92, 0.7, 0.46,
            0.26, 0.18, 0.15, 0.1,
        ]
        .into_iter()
        .map(|t| t * 200.0f32)
        .collect::<Vec<_>>();

        let clutch_max_torque = *engine_torque_curve
            .iter()
            .max_by_key(|x| (**x * 10000.0) as isize)
            .unwrap() as f32
            * 1.4;

        let tire_model = Rc::new(TireModel::default());

        Object::Car(CarObject {
            wheel_fl: Wheel::new(
                physics,
                &chassis,
                WHEEL_FRONT_OFFSET,
                false,
                false,
                tire_model.clone(),
            ),
            wheel_fr: Wheel::new(
                physics,
                &chassis,
                WHEEL_FRONT_OFFSET,
                true,
                false,
                tire_model.clone(),
            ),
            wheel_rl: Wheel::new(
                physics,
                &chassis,
                WHEEL_REAR_OFFSET,
                false,
                true,
                tire_model.clone(),
            ),
            wheel_rr: Wheel::new(physics, &chassis, WHEEL_REAR_OFFSET, true, true, tire_model),
            chassis,
            powertrain: CarPowertrain {
                engine: Engine {
                    velocity: 0.0,
                    inertia: 0.12,
                    torque_curve: engine_torque_curve,
                    min_velocity: r2v(500.0),
                    max_velocity: r2v(6000.0),
                    idle_velocity: r2v(640.0),
                    throttle: 0.0,
                    starter_engaged: false,
                    starter_torque: 60.0,
                    friction_torque: 30.0 / r2v(2000.0),
                    fuel_consumption: 0.21e-3 / r2v(3000.0),
                    load: 0.0,
                },
                clutch: Clutch::new(1.0, clutch_max_torque),
                fuel_tank: Fueltank::new_full(60.0),
                gearbox: Gearbox {
                    gear_ratios: vec![-3.7, 69420.0, 3.4, 2.2, 1.4, 1.0, 0.83, 0.71],
                    neutral_gear_index: 1,
                    gear: 1,
                    final_drive_ratio: 3.47,
                },
                throttle_input: 0.0,
            },
            brake_input: 0.0,
            shift_cooldown: 0.0,
            automatic_controls: true,
            velocity: Vec3::ZERO,
            acceleration: Vec3::ZERO,
        })
    }

    pub fn car(&mut self) -> &mut CarObject {
        match self {
            Object::Car(car) => car,
            _ => unreachable!(),
        }
    }
}
