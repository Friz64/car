use super::{Object, ObjectPart, ObjectPartPhysics, ObjectPartRenderInfo};
use crate::{graphics::cube::CubePass, physics::Physics};
use glam::{Quat, Vec3};
use nalgebra::{Isometry, Point3, UnitQuaternion};
use rapier3d::{
    dynamics::{RigidBodyBuilder, RigidBodyType},
    geometry::ColliderBuilder,
};

pub enum CubeType {
    Static,
    Dynamic { density: f32 },
}

pub struct CubeObject(pub ObjectPart);

impl CubeObject {
    pub fn draw(&mut self, physics: &mut Physics, cube_pass: &mut CubePass) {
        self.0.update(physics);
        cube_pass.add_instance(self.0.render_info().model_mat());
    }
}

impl Object {
    pub fn new_cube(
        physics: &mut Physics,
        translation: Vec3,
        rotation: Quat,
        size: Vec3,
        ty: CubeType,
    ) -> Object {
        let body_ty = match ty {
            CubeType::Static => RigidBodyType::Static,
            CubeType::Dynamic { .. } => RigidBodyType::Dynamic,
        };

        let rigidbody = RigidBodyBuilder::new(body_ty)
            .position(Isometry::from_parts(
                Point3::from(translation).into(),
                UnitQuaternion::from_quaternion(rotation.into()),
            ))
            .build();

        let collider = ColliderBuilder::cuboid(size.x * 0.5, size.y * 0.5, size.z * 0.5)
            .density(match ty {
                CubeType::Static => 1.0,
                CubeType::Dynamic { density } => density,
            })
            .build();

        Object::Cube(CubeObject(ObjectPart::new(
            Some(ObjectPartRenderInfo::new(translation, rotation, size)),
            Some(ObjectPartPhysics::new(physics, rigidbody, Some(collider))),
        )))
    }
}
