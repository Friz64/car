use super::{Object, ObjectPart, ObjectPartPhysics, ObjectPartRenderInfo};
use crate::{
    app::ApplicationState,
    graphics::{
        textured::{TexturedModel, TexturedPass},
        Models,
    },
    physics::Physics,
};
use glam::{Mat4, Quat, Vec3};
use nalgebra::{Isometry, Point3, UnitQuaternion};
use rapier3d::prelude::{Collider, RigidBodyBuilder, RigidBodyType};
use winit::dpi::PhysicalSize;

#[derive(Debug, Clone, Copy)]
pub enum PortalType {
    Twitter,
    Blog,
}

impl PortalType {
    pub fn url(&self) -> &str {
        match self {
            PortalType::Twitter => "https://twitter.com/Friz64_",
            PortalType::Blog => "https://blog.friz64.de/",
        }
    }

    pub fn model<'a>(&self, models: &'a Models) -> &'a TexturedModel {
        match self {
            PortalType::Twitter => &models.twitter,
            PortalType::Blog => &models.blog,
        }
    }
}

pub struct PortalObject {
    pub ty: PortalType,
    pub object_part: ObjectPart,
}

impl PortalObject {
    pub fn draw(
        &mut self,
        app: &mut ApplicationState,
        physics: &mut Physics,
        textured_pass: &mut TexturedPass,
        window_size: PhysicalSize<u32>,
        view_projection: Mat4,
        models: &Models,
    ) {
        self.object_part.update(physics);
        textured_pass.draw(
            app,
            window_size,
            view_projection,
            self.ty.model(models),
            &[self.object_part.render_info().model_mat()],
        );
    }
}

impl Object {
    pub fn new_portal(
        ty: PortalType,
        physics: &mut Physics,
        translation: Vec3,
        rotation: Quat,
        collider: Collider,
    ) -> Object {
        let rigidbody = RigidBodyBuilder::new(RigidBodyType::Static)
            .position(Isometry::from_parts(
                Point3::from(translation).into(),
                UnitQuaternion::from_quaternion(rotation.into()),
            ))
            .build();

        let object_part = ObjectPart::new(
            Some(ObjectPartRenderInfo::new(
                translation,
                rotation,
                Vec3::ONE * 20.0,
            )),
            Some(ObjectPartPhysics::new(physics, rigidbody, Some(collider))),
        );

        Object::Portal(PortalObject { ty, object_part })
    }

    pub fn portal(&mut self) -> &mut PortalObject {
        match self {
            Object::Portal(portal) => portal,
            _ => unreachable!(),
        }
    }
}
