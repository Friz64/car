use std::fmt::{Display, Write};

pub struct CarPowertrain {
    pub engine: Engine,
    pub clutch: Clutch,
    pub fuel_tank: Fueltank,
    pub gearbox: Gearbox,
    pub throttle_input: f32,
}

impl CarPowertrain {
    pub fn update(&mut self, dt: f32, wheel_speeds: &[f32], wheel_inertia_sum: f32) -> f32 {
        // differential gearbox takes avg
        let wheels_speed = wheel_speeds.iter().sum::<f32>() / wheel_speeds.len() as f32;

        // update velocities from last to first

        self.clutch.engine_velocity = self.engine.velocity;

        let gearbox_shaft_velocity = if let Some(gear_ratio) = self.gearbox.gear_ratio() {
            wheels_speed * gear_ratio * self.gearbox.final_drive_ratio
        } else {
            // gearbox has no inertia
            self.clutch.engine_velocity
        };
        self.clutch.gearbox_velocity = gearbox_shaft_velocity;

        let gearbox_inertia = self
            .gearbox
            .gear_ratio()
            .map(|gear_ratio| wheel_inertia_sum / gear_ratio / self.gearbox.final_drive_ratio)
            .unwrap_or(
                /* value doesnt matter because clutch is inactive in neutral*/ 1.0,
            );

        self.engine.update(
            &mut self.fuel_tank,
            &mut self.clutch,
            self.throttle_input,
            gearbox_inertia,
            dt,
        );

        self.gearbox
            .gear_ratio()
            .map(|gear_ratio| {
                -self.clutch.to_gearbox_torque * gear_ratio * self.gearbox.final_drive_ratio
            })
            .unwrap_or(0.0)
    }
}

pub struct Fueltank {
    pub capacity: f32,
    pub content: f32,
}

impl Fueltank {
    pub fn new_full(capacity: f32) -> Self {
        Self {
            capacity,
            content: capacity,
        }
    }
}

pub struct Engine {
    /// rad/s
    pub velocity: f32,
    /// Nm/rad/s²
    pub inertia: f32,
    pub torque_curve: Vec<f32>,
    pub min_velocity: f32,
    pub max_velocity: f32,
    /// basically carburator idle setting, should be higher than min_velocity
    pub idle_velocity: f32,
    pub throttle: f32,
    pub starter_engaged: bool,
    pub starter_torque: f32,
    /// in Nm/rad/s
    pub friction_torque: f32,
    /// in L/rad/s/throttle (liter per angular velocity per throttle)
    pub fuel_consumption: f32,
    /// current engine load, in range i0..1
    pub load: f32,
}

impl Engine {
    pub fn update(
        &mut self,
        fuel_tank: &mut Fueltank,
        clutch: &mut Clutch,
        throttle_input: f32,
        gearbox_inertia: f32,
        dt: f32,
    ) {
        if self.starter_engaged {
            if self.velocity > self.idle_velocity {
                self.starter_engaged = false;
            } else {
                self.throttle = 1.0;
            }
        } else {
            self.throttle = throttle_input;

            if self.velocity > self.min_velocity && self.velocity < self.idle_velocity {
                // 1 for stalling, 0 for idling
                let stalledness = 1.0
                    - (self.velocity - self.min_velocity)
                        / (self.idle_velocity - self.min_velocity);
                self.throttle += stalledness;
            }
        }

        self.throttle = self.throttle.min(1.0).max(0.0);

        if self.velocity >= self.min_velocity {
            fuel_tank.content -= self.throttle.powi(2) * self.velocity * self.fuel_consumption;

            if fuel_tank.content < 0.0 {
                fuel_tank.content = 0.0;
            }
        }

        let current_torque = self.get_torque() * self.throttle;

        let friction_torque =
            (self.friction_torque * self.velocity).min(self.inertia * self.velocity / dt);

        self.velocity += dt * (current_torque - friction_torque) / self.inertia;

        clutch.update(dt, self.velocity, self.inertia, gearbox_inertia);

        let clutch_torque = clutch.to_engine_torque;

        self.load = (clutch_torque / current_torque).min(1.0).max(0.0);

        self.velocity -= dt * clutch_torque / self.inertia;

        if self.velocity < 0.0 {
            self.velocity = 0.0;
        }
    }

    pub fn get_torque(&self) -> f32 {
        let starter_torque = if self.starter_engaged {
            self.starter_torque
        } else {
            0.0
        };

        if self.velocity >= self.min_velocity && self.velocity <= self.max_velocity {
            let len = self.torque_curve.len() - 1;
            let i = len as f32 * (self.velocity - self.min_velocity)
                / (self.max_velocity - self.min_velocity);

            let lower = i.floor() as usize;
            let higher = i.ceil() as usize;
            let mix = i.fract();

            self.torque_curve[lower] * (1.0 - mix)
                + self.torque_curve[higher] * mix
                + starter_torque
        } else {
            starter_torque
        }
    }

    pub fn rpm(&self) -> f32 {
        self.velocity * 60.0 / std::f32::consts::TAU
    }
}

pub struct Clutch {
    engine_velocity: f32,
    gearbox_velocity: f32,
    /// 0..1, 1 means engaged
    pub clutch_coeff: f32,
    max_lock_torque: f32,
    /// in Nm/rad/s
    lock_damping: f32,

    jitter_coeff: f32,
    last_diff: f32,

    to_engine_torque: f32,
    to_gearbox_torque: f32,
}

impl Clutch {
    pub fn new(lock_damping: f32, max_lock_torque: f32) -> Self {
        Clutch {
            engine_velocity: 0.0,
            gearbox_velocity: 0.0,
            lock_damping,
            max_lock_torque,
            clutch_coeff: 1.0,

            jitter_coeff: 1.0,
            last_diff: 0.0,

            to_engine_torque: 0.0,
            to_gearbox_torque: 0.0,
        }
    }

    pub fn update(
        &mut self,
        dt: f32,
        engine_velocity: f32,
        engine_inertia: f32,
        gearbox_inertia: f32,
    ) {
        let diff = self.engine_velocity - self.gearbox_velocity;
        let lock_damp = clamp_abs(
            diff * self.clutch_coeff * self.lock_damping,
            self.max_lock_torque,
        );

        if self.last_diff.signum() != diff.signum() {
            self.jitter_coeff *= 0.9;
        } else {
            self.jitter_coeff = (self.jitter_coeff * 1.2).min(1.0);
        }

        self.to_engine_torque = lock_damp
            .min((diff * engine_inertia).abs() * 0.5 / dt)
            .max(-engine_velocity * engine_inertia / dt);
        self.to_gearbox_torque = clamp_abs(-lock_damp, (diff * gearbox_inertia).abs() * 0.5 / dt);
    }
}

pub struct Gearbox {
    // neutral gear's ratio is ignored
    pub gear_ratios: Vec<f32>,
    pub neutral_gear_index: usize,
    pub gear: usize,
    pub final_drive_ratio: f32,
}

impl Gearbox {
    pub fn gear_ratio(&self) -> Option<f32> {
        if self.gear == self.neutral_gear_index {
            None
        } else {
            Some(self.gear_ratios[self.gear])
        }
    }
}

impl Display for Gearbox {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.gear > self.neutral_gear_index {
            (self.gear - self.neutral_gear_index).fmt(f)
        } else if self.gear == self.neutral_gear_index {
            f.write_char('N')
        } else if self.neutral_gear_index > 1 {
            write!(f, "R{}", self.neutral_gear_index - self.gear)
        } else {
            f.write_char('R')
        }
    }
}

pub fn clamp_abs(x: f32, abs_max: f32) -> f32 {
    x.signum() * x.abs().min(abs_max)
}
