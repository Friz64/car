pub mod cube;
pub mod joystick;
pub mod textured;

use self::textured::TexturedModel;
use crate::WebGl;
use anyhow::{anyhow, bail, Context, Result};
pub use web_sys::{WebGlBuffer, WebGlProgram, WebGlRenderingContext as WebGlConsts};

pub struct Models {
    pub car: TexturedModel,
    pub wheel: TexturedModel,
    pub twitter: TexturedModel,
    pub blog: TexturedModel,
}

impl Models {
    pub fn new(webgl: &WebGl) -> Result<Models> {
        Ok(Models {
            car: TexturedModel::from_obj(
                webgl,
                include_bytes!("../assets/car.obj"),
                "/assets/car.png",
                false,
            )
            .context("Failed to load car TexturedModel")?,
            wheel: TexturedModel::from_obj(
                webgl,
                include_bytes!("../assets/wheel.obj"),
                "/assets/wheel.png",
                false,
            )
            .context("Failed to load wheel TexturedModel")?,
            twitter: TexturedModel::from_obj(
                webgl,
                include_bytes!("../assets/twitter.obj"),
                "/assets/twitter.png",
                true,
            )
            .context("Failed to load twitter TexturedModel")?,
            blog: TexturedModel::from_obj(
                webgl,
                include_bytes!("../assets/friz64blog3d.obj"),
                "/assets/friz64blog3d.png",
                true,
            )
            .context("Failed to load blog TexturedModel")?,
        })
    }
}

fn compile_program(webgl: &WebGl, vs: &str, fs: &str) -> Result<WebGlProgram> {
    let program = webgl
        .create_program()
        .ok_or_else(|| anyhow!("Failed to create program"))?;

    let add_shader = |ty, src| -> Result<_> {
        let shader = webgl
            .create_shader(ty)
            .ok_or_else(|| anyhow!("Failed to create shader object"))?;
        webgl.shader_source(&shader, src);
        webgl.compile_shader(&shader);

        let success = webgl
            .get_shader_parameter(&shader, WebGlConsts::COMPILE_STATUS)
            .as_bool()
            .unwrap_or(false);

        if !success {
            match webgl.get_shader_info_log(&shader) {
                Some(info_log) => bail!("{}", info_log),
                None => bail!("Unknown shader compilation error"),
            }
        }

        webgl.attach_shader(&program, &shader);
        Ok(())
    };

    add_shader(WebGlConsts::VERTEX_SHADER, vs).context("Failed to add vertex shader")?;
    add_shader(WebGlConsts::FRAGMENT_SHADER, fs).context("Failed to add fragment shader")?;
    webgl.link_program(&program);

    let success = webgl
        .get_program_parameter(&program, WebGlConsts::LINK_STATUS)
        .as_bool()
        .unwrap_or(false);

    if !success {
        match webgl.get_program_info_log(&program) {
            Some(info_log) => bail!("{}", info_log),
            None => bail!("Unknown program linking error"),
        }
    }

    Ok(program)
}
