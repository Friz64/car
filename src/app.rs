use crate::{
    graphics::{
        cube::CubePass, joystick::JoystickPass, textured::TexturedPass, Models, WebGlConsts,
    },
    object::{Objects, PortalType},
    physics::Physics,
    powertrain::clamp_abs,
    sounds::Sounds,
    util, WebGl,
};
use anyhow::{Context, Result};
use glam::{Mat4, Quat, Vec2, Vec3};
use std::collections::HashSet;
use std::sync::Mutex;
use web_sys::{Document, Window};
use winit::dpi::PhysicalSize;
use winit::event::VirtualKeyCode;

pub struct ApplicationState {
    pub webgl: WebGl,
}

pub struct Application {
    pub web_window: Window,
    pub document: Document,
    pub sounds: Sounds,
    pub cube_pass: CubePass,
    pub textured_pass: TexturedPass,
    pub joystick_pass: JoystickPass,
    pub physics: Physics,
    pub models: Models,
    pub objects: Objects,
    pub state: ApplicationState,
    pub cam_pos: Vec3,
    pub collided_portal: Mutex<Option<PortalType>>,
    pub navigated: bool,
    pub steering: f32,
}

impl Application {
    pub fn new(webgl: WebGl, web_window: Window, document: Document) -> Result<Application> {
        webgl.clear_color(0.235, 0.235, 0.235, 1.0);

        let mut physics = Physics::new();
        let objects = Objects::new(&mut physics);

        Ok(Application {
            web_window,
            document,
            sounds: Sounds::new().context("Failed to load sounds")?,
            cube_pass: CubePass::new(&webgl).context("Failed to set up CubePass")?,
            textured_pass: TexturedPass::new(&webgl).context("Failed to set up TexturedPass")?,
            joystick_pass: JoystickPass::new(&webgl).context("Failed to set up JoystickPass")?,
            physics,
            models: Models::new(&webgl).context("Failed to load models")?,
            objects,
            state: ApplicationState { webgl },
            cam_pos: Vec3::new(0.0, -6.0, 0.0),
            collided_portal: Mutex::new(None),
            navigated: false,
            steering: 0.0,
        })
    }

    pub fn frame(
        &mut self,
        delta_time: f32,
        window_size: PhysicalSize<u32>,
        dragging: Vec2,
        pressed_keys: &HashSet<VirtualKeyCode>,
    ) -> Result<()> {
        let dragging = (dragging * 8.0).max(-Vec2::ONE).min(Vec2::ONE);

        let projection = Mat4::perspective_infinite_reverse_rh(
            80f32.to_radians(),
            window_size.width as f32 / window_size.height as f32,
            0.1,
        );

        self.physics
            .frame(delta_time, &mut self.objects, &self.collided_portal);

        let collided_portal = *self.collided_portal.lock().unwrap();
        match collided_portal {
            Some(ty) => self.navigate(ty.url())?,
            None => (),
        }

        let kerrek_pos = self.update_kerrek(dragging, delta_time, pressed_keys);
        let view = Mat4::look_at_rh(self.cam_pos, kerrek_pos, Vec3::new(0.0, 1.0, 0.0));
        let view_projection = projection * view;

        self.state
            .webgl
            .clear(WebGlConsts::COLOR_BUFFER_BIT | WebGlConsts::DEPTH_BUFFER_BIT);
        for (_idx, obj) in self.objects.slab.iter_mut() {
            obj.draw(
                &mut self.state,
                window_size,
                view_projection,
                &mut self.models,
                &mut self.physics,
                &mut self.cube_pass,
                &mut self.textured_pass,
            );
        }

        self.cube_pass
            .draw(&mut self.state, window_size, view_projection);
        self.state.webgl.clear(WebGlConsts::DEPTH_BUFFER_BIT);
        self.joystick_pass
            .draw(&mut self.state, window_size, dragging);

        Ok(())
    }

    fn navigate(&mut self, url: &str) -> Result<()> {
        if !self.navigated {
            self.navigated = true;

            let _ = self
                .sounds
                .boom
                .play()
                .map_err(util::js_value_to_err)
                .context("Failed to play dong sound")?;

            self.web_window
                .location()
                .assign(url)
                .map_err(util::js_value_to_err)
                .context("Failed to assign URL")?;
        }

        Ok(())
    }

    // returns the kerrek's position
    fn update_kerrek(
        &mut self,
        mut dragging: Vec2,
        delta_time: f32,
        pressed_keys: &HashSet<VirtualKeyCode>,
    ) -> Vec3 {
        let mut kerrek = self.objects.slab[self.objects.kerrek].car();
        let kerrek_physics = kerrek.chassis.physics_component();
        let (kerrek_pos, kerrek_rotation) = kerrek_physics.translation_rotation(&mut self.physics);

        let cam_distance = 6.0;
        let cam_offset = kerrek_rotation * Vec3::new(0.0, 1.8, -1.0);
        self.cam_pos = (self.cam_pos - kerrek_pos - cam_offset).normalize() * cam_distance
            + kerrek_pos
            + cam_offset;

        let drive_wheels_speed =
            (kerrek.wheel_rl.angular_velocity - kerrek.wheel_rr.angular_velocity) * 0.5;

        if kerrek.automatic_controls {
            if kerrek.powertrain.gearbox.gear < kerrek.powertrain.gearbox.neutral_gear_index {
                dragging.y *= -1.0;
            }

            if dragging.y < -0.5
                && kerrek.powertrain.engine.velocity < kerrek.powertrain.engine.min_velocity
            {
                kerrek.powertrain.engine.starter_engaged = true;
            }

            kerrek.shift_cooldown = (kerrek.shift_cooldown - delta_time).max(0.0);

            if kerrek.shift_cooldown == 0.0
                && (kerrek.wheel_fr.is_grounded
                    || kerrek.wheel_fl.is_grounded
                    || kerrek.wheel_rr.is_grounded
                    || kerrek.wheel_rl.is_grounded)
            {
                if kerrek.powertrain.gearbox.gear
                    >= kerrek.powertrain.gearbox.neutral_gear_index + 1
                {
                    if kerrek.powertrain.engine.velocity
                        > kerrek.powertrain.engine.max_velocity * 0.7
                    {
                        if kerrek.powertrain.gearbox.gear + 1
                            < kerrek.powertrain.gearbox.gear_ratios.len()
                        {
                            kerrek.powertrain.gearbox.gear += 1;
                            kerrek.powertrain.clutch.clutch_coeff = 0.0;
                            kerrek.shift_cooldown = 0.5;
                        }
                    }

                    if kerrek.powertrain.engine.velocity
                        < kerrek.powertrain.engine.idle_velocity * 2.0
                    {
                        if kerrek.powertrain.gearbox.gear
                            > kerrek.powertrain.gearbox.neutral_gear_index + 1
                        {
                            kerrek.powertrain.gearbox.gear -= 1;
                            kerrek.powertrain.clutch.clutch_coeff = 0.0;
                            kerrek.shift_cooldown = 0.5;
                        }
                    }

                    if kerrek.powertrain.gearbox.gear
                        == kerrek.powertrain.gearbox.neutral_gear_index + 1
                        && dragging.y > 0.5
                        && drive_wheels_speed < 6.0
                    {
                        // shift to neutral
                        kerrek.powertrain.gearbox.gear -= 1;
                        kerrek.shift_cooldown = 0.2;
                    }
                } else if kerrek.powertrain.gearbox.gear
                    == kerrek.powertrain.gearbox.neutral_gear_index
                {
                    if dragging.y < -0.5 && drive_wheels_speed > -6.0 {
                        kerrek.powertrain.gearbox.gear += 1;
                        kerrek.shift_cooldown = 0.5;
                    }
                    if dragging.y > 0.5 && drive_wheels_speed < 6.0 {
                        kerrek.powertrain.gearbox.gear -= 1;
                        kerrek.powertrain.clutch.clutch_coeff = 0.0;
                        kerrek.shift_cooldown = 0.8;
                    }
                } else {
                    // reverse
                    if dragging.y > 0.5 && drive_wheels_speed > -6.0 {
                        // shift to neutral
                        kerrek.powertrain.gearbox.gear += 1;
                        kerrek.shift_cooldown = 0.2;
                    }
                }
            }
        }

        kerrek.powertrain.throttle_input = (-dragging.y).max(0.0);
        kerrek.brake_input = dragging.y.max(0.0) * 250.0;

        // slowly engage clutch
        kerrek.powertrain.clutch.clutch_coeff =
            (kerrek.powertrain.clutch.clutch_coeff + delta_time / 0.5).min(1.0);

        if kerrek.powertrain.throttle_input < 0.01
            && kerrek.powertrain.engine.velocity < kerrek.powertrain.engine.idle_velocity * 1.2
        {
            kerrek.powertrain.clutch.clutch_coeff = 0.0;
        }

        // rev up
        if kerrek.powertrain.throttle_input > 0.01
            && kerrek.powertrain.gearbox.gear != kerrek.powertrain.gearbox.neutral_gear_index
            && kerrek.powertrain.engine.velocity < kerrek.powertrain.engine.idle_velocity * 2.0
        {
            kerrek.powertrain.clutch.clutch_coeff = 0.0;
        }

        if pressed_keys.get(&VirtualKeyCode::K).is_some() {
            kerrek.powertrain.engine.starter_engaged ^= true;
        }

        if pressed_keys.get(&VirtualKeyCode::I).is_some() {
            kerrek.automatic_controls ^= true;
        }

        if pressed_keys.get(&VirtualKeyCode::P).is_some() {
            if kerrek.powertrain.gearbox.gear + 1 < kerrek.powertrain.gearbox.gear_ratios.len() {
                kerrek.powertrain.gearbox.gear += 1;
                kerrek.powertrain.clutch.clutch_coeff = 0.0;
            }
        }
        if pressed_keys.get(&VirtualKeyCode::O).is_some() {
            if kerrek.powertrain.gearbox.gear > 0 {
                kerrek.powertrain.gearbox.gear -= 1;
                kerrek.powertrain.clutch.clutch_coeff = 0.0;
            }
        }

        // deg/s
        let base_steering_rate = 80.0;
        let steering_angle = 40.0;
        let counter_steering_fac = 3.0;

        let steering_rate = if self.steering.signum() != dragging.x && dragging.x.abs() > 0.01 {
            counter_steering_fac
        } else {
            1.0
        } * base_steering_rate
            / steering_angle
            / ((drive_wheels_speed + 20.0) / 40.0).max(1.0);

        self.steering =
            clamp_abs(dragging.x - self.steering, steering_rate * delta_time) + self.steering;

        kerrek.wheel_fl.axle_direction =
            Quat::from_rotation_y(self.steering * -steering_angle.to_radians()) * -Vec3::X;
        kerrek.wheel_fr.axle_direction =
            Quat::from_rotation_y(self.steering * -steering_angle.to_radians()) * Vec3::X;

        let car_info_paragraph = self.document.get_element_by_id("car-info").unwrap();

        car_info_paragraph.set_inner_html(&format!(
            "avg wheel speed: {:.2} km/h<br>engine speed(K): {:.0} RPM<br>gear(O/P): {}<br>clutch: {:.2}<br>fuel: {:.6} L<br>torque at wheels: {:.1} Nm<br>car speed: {:.2} m/s<br>car acceleration: {:.2} m/s²<br>automatic controls(I): {}",
            kerrek.avg_wheel_speed(),
            kerrek.powertrain.engine.rpm(),
            kerrek.powertrain.gearbox,
            kerrek.powertrain.clutch.clutch_coeff,
            kerrek.powertrain.fuel_tank.content,
            kerrek.wheel_fr.motor*kerrek.wheel_fr.side_factor()+
                kerrek.wheel_fl.motor*kerrek.wheel_fl.side_factor()+
                kerrek.wheel_rr.motor*kerrek.wheel_rr.side_factor()+
                kerrek.wheel_rl.motor*kerrek.wheel_rl.side_factor(),
            kerrek.velocity.length(),
            kerrek.acceleration.length(),
            kerrek.automatic_controls,
        ));

        kerrek_pos
    }
}
