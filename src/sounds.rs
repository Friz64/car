use crate::util;
use anyhow::{Context, Result};
use web_sys::HtmlAudioElement;

pub struct Sounds {
    pub boom: HtmlAudioElement,
}

impl Sounds {
    pub fn new() -> Result<Sounds> {
        let load_sound = |url: &str| -> Result<HtmlAudioElement> {
            Ok(HtmlAudioElement::new_with_src(url)
                .map_err(util::js_value_to_err)
                .with_context(|| format!("Failed to load {:?} sound", url))?)
        };

        Ok(Sounds {
            boom: load_sound("assets/boom.ogg")?,
        })
    }
}
