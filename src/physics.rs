use crate::object::{Objects, PortalType};
use rapier3d::{
    dynamics::{CCDSolver, IntegrationParameters, JointSet, RigidBodySet},
    geometry::{BroadPhase, ColliderSet, NarrowPhase},
    pipeline::{PhysicsPipeline, QueryPipeline},
    prelude::{ColliderHandle, ContactEvent, ContactPair, EventHandler, IslandManager},
};
use std::sync::Mutex;

pub const GRAVITY: f32 = 9.81;

struct PhysicsEventHandler<'a> {
    collided_portal: &'a Mutex<Option<PortalType>>,
    kerrek_chassis_collider: ColliderHandle,
    twitter_portal_collider: ColliderHandle,
    blog_portal_collider: ColliderHandle,
}

impl<'a> EventHandler for PhysicsEventHandler<'a> {
    fn handle_intersection_event(&self, _event: rapier3d::prelude::IntersectionEvent) {}

    fn handle_contact_event(&self, event: ContactEvent, _contact_pair: &ContactPair) {
        if let ContactEvent::Started(a, b) = event {
            let other = if a == self.kerrek_chassis_collider {
                b
            } else if b == self.kerrek_chassis_collider {
                a
            } else {
                unreachable!()
            };

            let mut collided_portal = self.collided_portal.lock().unwrap();
            if other == self.twitter_portal_collider {
                *collided_portal = Some(PortalType::Twitter);
            } else if other == self.blog_portal_collider {
                *collided_portal = Some(PortalType::Blog);
            }
        }
    }
}

pub struct Physics {
    step_time_counter: f32,
    pub query_pipeline: QueryPipeline,
    pipeline: PhysicsPipeline,
    pub integration_parameters: IntegrationParameters,
    island_manager: IslandManager,
    broad_phase: BroadPhase,
    narrow_phase: NarrowPhase,
    pub bodies: RigidBodySet,
    pub colliders: ColliderSet,
    pub joints: JointSet,
    ccd_solver: CCDSolver,
}

impl Physics {
    pub fn new() -> Physics {
        Physics {
            step_time_counter: 0.0,
            query_pipeline: QueryPipeline::new(),
            pipeline: PhysicsPipeline::new(),
            integration_parameters: IntegrationParameters {
                dt: 1.0 / 38.0,
                //max_velocity_iterations: 20,
                //max_position_iterations: 10,
                ..Default::default()
            },
            island_manager: IslandManager::new(),
            broad_phase: BroadPhase::new(),
            narrow_phase: NarrowPhase::new(),
            bodies: RigidBodySet::new(),
            colliders: ColliderSet::new(),
            joints: JointSet::new(),
            ccd_solver: CCDSolver::new(),
        }
    }

    pub fn frame(
        &mut self,
        delta_time: f32,
        objects: &mut Objects,
        collided_portal: &Mutex<Option<PortalType>>,
    ) {
        self.step_time_counter += delta_time;
        while self.step_time_counter >= self.integration_parameters.dt {
            let kerrek_chassis_collider = objects.slab[objects.kerrek]
                .car()
                .chassis
                .physics_component()
                .collider_handle
                .unwrap();
            let twitter_portal_collider = objects.slab[objects.twitter_portal]
                .portal()
                .object_part
                .physics_component()
                .collider_handle
                .unwrap();
            let blog_portal_collider = objects.slab[objects.blog_portal]
                .portal()
                .object_part
                .physics_component()
                .collider_handle
                .unwrap();

            self.pipeline.step(
                &[0.0, -GRAVITY, 0.0].into(),
                &self.integration_parameters,
                &mut self.island_manager,
                &mut self.broad_phase,
                &mut self.narrow_phase,
                &mut self.bodies,
                &mut self.colliders,
                &mut self.joints,
                &mut self.ccd_solver,
                &(),
                &PhysicsEventHandler {
                    collided_portal,
                    kerrek_chassis_collider,
                    twitter_portal_collider,
                    blog_portal_collider,
                },
            );

            self.query_pipeline
                .update(&self.island_manager, &self.bodies, &self.colliders);

            self.post_physics_step(self.integration_parameters.dt, objects);

            self.step_time_counter -= self.integration_parameters.dt;
        }
    }

    fn post_physics_step(&mut self, dt: f32, objects: &mut Objects) {
        let car = objects.slab[objects.kerrek].car();
        car.post_physics_step(dt, self);
    }
}
