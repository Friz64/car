mod app;
mod graphics;
mod object;
mod physics;
mod powertrain;
mod sounds;

use crate::app::Application;
use anyhow::{anyhow, Context, Result};
use glam::Vec2;
use instant::Instant;
use std::collections::HashSet;
use std::{cmp, collections::HashMap, ops::Deref, rc::Rc};
use wasm_bindgen::{prelude::*, JsCast};
use web_sys::{AngleInstancedArrays, HtmlCanvasElement, WebGlRenderingContext};
use winit::{
    dpi::{LogicalSize, PhysicalPosition, PhysicalSize},
    event::{DeviceId, ElementState, Event, MouseButton, VirtualKeyCode, WindowEvent},
    event_loop::{ControlFlow, EventLoop},
    platform::web::WindowExtWebSys,
    window::WindowBuilder,
};

mod util {
    use anyhow::anyhow;
    use wasm_bindgen::JsValue;

    pub fn js_value_to_err(js_value: JsValue) -> anyhow::Error {
        let as_string = js_value.as_string();
        anyhow!("{}", as_string.unwrap_or_else(|| format!("{:?}", js_value)))
    }
}

#[derive(Clone)]
pub struct WebGl {
    pub webgl_ctx: WebGlRenderingContext,
    pub angle_instanced_arrays: AngleInstancedArrays,
}

impl WebGl {
    fn new(canvas: &HtmlCanvasElement) -> Result<WebGl> {
        let webgl_ctx = canvas
            .get_context("webgl")
            .map_err(util::js_value_to_err)?
            .ok_or_else(|| anyhow!("Failed to get WebGL1 context"))?
            .dyn_into::<WebGlRenderingContext>()
            .unwrap();

        let angle_instanced_arrays = webgl_ctx
            .get_extension("ANGLE_instanced_arrays")
            .map_err(util::js_value_to_err)?
            .ok_or_else(|| anyhow!("Failed to get ANGLE_instanced_arrays ext"))?
            .unchecked_into::<AngleInstancedArrays>();

        Ok(WebGl {
            webgl_ctx,
            angle_instanced_arrays,
        })
    }
}

impl Deref for WebGl {
    type Target = WebGlRenderingContext;

    fn deref(&self) -> &Self::Target {
        &self.webgl_ctx
    }
}

#[derive(Debug)]
struct Dragging {
    device_id: DeviceId,
    start_pos: PhysicalPosition<f64>,
    current_pos: PhysicalPosition<f64>,
}

impl Dragging {
    fn normalized_dir(&self, window_size: PhysicalSize<u32>) -> Vec2 {
        let start_pos = Vec2::new(self.start_pos.x as f32, self.start_pos.y as f32);
        let current_pos = Vec2::new(self.current_pos.x as f32, self.current_pos.y as f32);
        (current_pos - start_pos) / cmp::min(window_size.width, window_size.height) as f32
    }
}

fn run() -> Result<()> {
    console_log::init()?;

    let web_window = web_sys::window().ok_or_else(|| anyhow!("Failed to get web_sys window"))?;
    let document = web_window
        .document()
        .ok_or_else(|| anyhow!("Failed to get web_sys document"))?;
    let app_element = document
        .get_element_by_id("app")
        .ok_or_else(|| anyhow!("Failed to get app element"))?;

    let current_app_element_size = {
        let app_element = app_element.clone();
        move || LogicalSize::new(app_element.client_width(), app_element.client_height())
    };

    let event_loop = EventLoop::new();
    let winit_window = WindowBuilder::new()
        .with_inner_size(current_app_element_size())
        .build(&event_loop)
        .context("Failed to create winit window")?;

    let canvas = winit_window.canvas();
    app_element
        .append_child(&canvas)
        .map_err(util::js_value_to_err)
        .context("Failed to add canvas element to app")?;

    let window = Rc::new(winit_window);
    let closure = Closure::wrap(Box::new({
        let window = window.clone();
        move |_: web_sys::Event| window.set_inner_size(current_app_element_size())
    }) as Box<dyn FnMut(_)>);

    web_window
        .add_event_listener_with_callback("resize", closure.as_ref().unchecked_ref())
        .map_err(util::js_value_to_err)
        .context("Failed to add resize event listener")?;
    closure.forget();

    let webgl = WebGl::new(&canvas).context("Failed to initialize WebGL1")?;
    let mut app = Application::new(webgl, web_window, document.clone())
        .context("Failed to initialize Application struct")?;

    // remove loading text
    let loading_element = document
        .get_element_by_id("loading")
        .ok_or_else(|| anyhow!("Failed to get loading text"))?;
    loading_element.set_outer_html("");

    let mut window_size = window.inner_size();
    let mut last_iteration = Instant::now();
    let mut cursor_positions = HashMap::new();
    let mut cursor_dragging = None;
    let mut kb_up = false;
    let mut kb_down = false;
    let mut kb_left = false;
    let mut kb_right = false;

    let mut pressed_keys = HashSet::new();

    event_loop.run(move |event, _, control_flow| match event {
        Event::WindowEvent { window_id, event } if window.id() == window_id => match event {
            WindowEvent::Resized(new_size) => window_size = new_size,
            WindowEvent::KeyboardInput { input, .. } => {
                match input.virtual_keycode {
                    Some(VirtualKeyCode::W) | Some(VirtualKeyCode::Up) => {
                        kb_up = input.state == ElementState::Pressed;
                    }
                    Some(VirtualKeyCode::A) | Some(VirtualKeyCode::Left) => {
                        kb_left = input.state == ElementState::Pressed;
                    }
                    Some(VirtualKeyCode::S) | Some(VirtualKeyCode::Down) => {
                        kb_down = input.state == ElementState::Pressed;
                    }
                    Some(VirtualKeyCode::D) | Some(VirtualKeyCode::Right) => {
                        kb_right = input.state == ElementState::Pressed;
                    }
                    _ => (),
                }

                if let Some(virtual_keycode) = input.virtual_keycode {
                    match input.state {
                        ElementState::Pressed => {
                            pressed_keys.insert(virtual_keycode);
                        }
                        ElementState::Released => {
                            pressed_keys.remove(&virtual_keycode);
                        }
                    }
                }
            }
            WindowEvent::MouseInput {
                device_id,
                state: ElementState::Pressed,
                button: MouseButton::Left,
                ..
            } => {
                if cursor_dragging.is_none() {
                    let cursor_pos = *cursor_positions
                        .get(&device_id)
                        .expect("Cursor position not found");

                    cursor_dragging = Some(Dragging {
                        device_id,
                        start_pos: cursor_pos,
                        current_pos: cursor_pos,
                    });
                }
            }
            WindowEvent::CursorLeft { device_id }
            | WindowEvent::MouseInput {
                device_id,
                state: ElementState::Released,
                button: MouseButton::Left,
                ..
            } => {
                if let Some(cursor_dragging_inner) = &cursor_dragging {
                    if device_id == cursor_dragging_inner.device_id {
                        cursor_dragging = None;
                    }
                }
            }
            WindowEvent::CursorMoved {
                device_id,
                position,
                ..
            } => {
                *cursor_positions.entry(device_id).or_insert(position) = position;
                if let Some(cursor_dragging) = &mut cursor_dragging {
                    if device_id == cursor_dragging.device_id {
                        cursor_dragging.current_pos = position;
                    }
                }
            }
            _ => (),
        },
        Event::MainEventsCleared => {
            let this_iteration = Instant::now();
            let delta_time = (this_iteration - last_iteration).as_secs_f32();
            last_iteration = this_iteration;

            let mut dragging = Vec2::ZERO;
            if kb_up {
                dragging.y -= 1.0;
            }

            if kb_down {
                dragging.y += 1.0;
            }

            if kb_left {
                dragging.x -= 1.0;
            }

            if kb_right {
                dragging.x += 1.0;
            }

            if dragging.length_squared() < 0.1 {
                dragging += cursor_dragging
                    .as_ref()
                    .map(|d| d.normalized_dir(window_size))
                    .unwrap_or_default();
            }

            if let Err(err) = app.frame(delta_time, window_size, dragging, &pressed_keys) {
                log::error!("Failed to process Application frame:\n{:?}", err);
                *control_flow = ControlFlow::Exit;
            }

            pressed_keys.clear();
        }
        _ => (),
    })
}

#[wasm_bindgen(start)]
pub fn start() -> Result<(), JsValue> {
    console_error_panic_hook::set_once();
    run().map_err(|err| format!("\nRust error: {:?}", err).into())
}
